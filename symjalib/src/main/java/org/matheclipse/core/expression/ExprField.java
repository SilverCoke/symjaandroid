package org.matheclipse.core.expression;

import org.apache.commons.math4.Field;
import org.apache.commons.math4.FieldElement;
import org.apache.commons.math4.linear.ArrayFieldVector;
import org.apache.commons.math4.linear.BlockFieldMatrix;
import org.apache.commons.math4.linear.FieldMatrix;
import org.apache.commons.math4.linear.FieldVector;
import org.matheclipse.core.interfaces.IAST;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.core.interfaces.INumber;

/**
 * Class for representing a field of <code>IExpr</code>.
 * 
 * @see IExpr
 */
public class ExprField implements Field<IExpr> {
	public final static ExprField CONST = new ExprField();


	@Override
	public IExpr getOne() {
		return F.C1;
	}

	/** {@inheritDoc} */
	@Override
	public IExpr getZero() {
		return F.C0;
	}

	/** {@inheritDoc} */
	@Override
	public Class<? extends FieldElement<IExpr>> getRuntimeClass() {
		return IExpr.class;
	}

	/**
	 * Returns a FieldVector if possible.
	 * 
	 * @param listVector
	 * @return <code>null</code> if the <code>listVector</code> is no list
	 * @throws ClassCastException
	 * @throws IndexOutOfBoundsException
	 */
	public static FieldVector<IExpr> list2Vector(final IAST listVector) throws ClassCastException,
			IndexOutOfBoundsException {
		if (listVector == null) {
			return null;
		}
		if (!listVector.isList()) {
			return null;
		}

		final int rowSize = listVector.size() - 1;

		final IExpr[] elements = new IExpr[rowSize];
		for (int i = 0; i < rowSize; i++) {
			elements[i] = listVector.get(i + 1);
		}
		return new ArrayFieldVector<IExpr>(elements);
	}

	/**
	 * Returns a FieldMatrix if possible.
	 * 
	 * @param listMatrix
	 * @return <code>null</code> if the <code>listMatrix</code> is no list
	 * @throws ClassCastException
	 * @throws IndexOutOfBoundsException
	 */
	public static FieldMatrix<IExpr> list2Matrix(final IAST listMatrix) throws ClassCastException,
			IndexOutOfBoundsException {
		if (listMatrix == null) {
			return null;
		}
		if (!listMatrix.isList()) {
			return null;
		}

		IAST currInRow = (IAST) listMatrix.arg1();
		if (currInRow.size() == 1) {
			// special case 0-Matrix
			IExpr[][] array = new IExpr[0][0];
			return new BlockFieldMatrix<IExpr>(array);
		}
		final int rowSize = listMatrix.size() - 1;
		final int colSize = currInRow.size() - 1;

		final IExpr[][] elements = new IExpr[rowSize][colSize];
		for (int i = 1; i < rowSize + 1; i++) {
			currInRow = (IAST) listMatrix.get(i);
			if (currInRow.head() != F.List) {
				return null;
			}
			for (int j = 1; j < colSize + 1; j++) {
				elements[i - 1][j - 1] = currInRow.get(j);
			}
		}
		return new BlockFieldMatrix<IExpr>(elements);
	}

	/**
	 * Converts a FieldMatrix to the list expression representation.
	 * 
	 * @param matrix
	 * @return
	 */
	public static IAST matrix2List(final FieldMatrix<IExpr> matrix) {
		if (matrix == null) {
			return null;
		}
		final int rowSize = matrix.getRowDimension();
		final int colSize = matrix.getColumnDimension();

		final IAST out = F.List();
		IAST currOutRow;
		for (int i = 0; i < rowSize; i++) {
			currOutRow = F.List();
			out.add(currOutRow);
			for (int j = 0; j < colSize; j++) {
				IExpr expr = matrix.getEntry(i, j);
				if (expr instanceof INumber) {
					currOutRow.add(expr);
				} else {
					currOutRow.add(F.eval(F.Together(expr)));
				}
			}
		}
		out.addEvalFlags(IAST.IS_MATRIX);
		return out;
	}

	/**
	 * Convert a FieldVector to an IAST list.
	 * 
	 * @param vector
	 * @return
	 */
	public static IAST vector2List(final FieldVector<IExpr> vector) {
		if (vector == null) {
			return null;
		}
		final int rowSize = vector.getDimension();

		final IAST out = F.List();
		for (int i = 0; i < rowSize; i++) {
			out.add(vector.getEntry(i));
		}
		out.addEvalFlags(IAST.IS_VECTOR);
		return out;
	}

}
